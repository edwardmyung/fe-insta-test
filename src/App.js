import {useState, useEffect} from 'react'
import './App.css'

const API_URL_STEM = 'https://jsonplaceholder.typicode.com'

const PhotoListView = ({setSelectedPhoto}) => {
  const [photos, setPhotos] = useState([])
  useEffect(() => {
    const fetchPhotos = async () => {
      const res = await fetch(`${API_URL_STEM}/photos`)
      const data = await res.json()
      setPhotos(data)
    }

    fetchPhotos()
  }, [])

  return (
    <div className='container'>
      {photos.map(photo => {
        return (
          <img
            key={photo.id}
            src={photo.thumbnailUrl}
            className='photo'
            alt={photo.title}
            onClick={() => {
              setSelectedPhoto(photo)
            }}
          />
        )
      })}
    </div>
  )
}

const PhotoView = ({selectedPhoto, setSelectedPhoto}) => {
  const [comments, setComments] = useState([])
  useEffect(() => {
    const fetchCommentsForPhoto = async () => {
      const res = await fetch(`${API_URL_STEM}/comments?postId=${selectedPhoto.id}`)
      const data = await res.json()
      setComments(data)
    }

    fetchCommentsForPhoto()
  }, [selectedPhoto.id])
  return (
    <div className='container' style={{maxWidth: 500}}>
      <button onClick={() => setSelectedPhoto(null)}>Go back</button>
      <img src={selectedPhoto.thumbnailUrl} className='photo--full-width' alt={selectedPhoto.title} />
      {JSON.stringify(comments)}
    </div>
  )
}

function App() {
  const [selectedPhoto, setSelectedPhoto] = useState(null)
  return selectedPhoto ? (
    <PhotoView selectedPhoto={selectedPhoto} setSelectedPhoto={setSelectedPhoto} />
  ) : (
    <PhotoListView setSelectedPhoto={setSelectedPhoto} />
  )
}

export default App
